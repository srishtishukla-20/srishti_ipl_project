const csvtojson=require("csvtojson");
const FileSystem=require("fs")
const csvfilepath1="matches.csv"


csvtojson()
.fromFile(csvfilepath1)
.then((json)=>{
    array1=json
    // console.log(array1)
    // 1. Number of matches played per year for all the years in IPL.
    function toss_winner(){

    let matches_won_toss = {};
    let i=0;
    while(i<array1.length){
        if (matches_won_toss.hasOwnProperty(array1[i]["season"])){
            matches_won_toss[array1[i]["season"]] += 1;
        }
        else{
            matches_won_toss[array1[i]["season"]] = 1;
        }
        i++
    }
    
    return matches_won_toss;
}

    FileSystem.writeFileSync("matches.json",JSON.stringify(json),"utf-8",(err)=>{
        if(err) console.log(err)
    })
    console.log("question no 1:",toss_winner());

})

csvtojson()
.fromFile(csvfilepath1)
.then((json)=>{
    array1=json
    
    function players_won (){
        
        let team = {};
        let index=0;
        while(index<array1.length){
            let season = array1[index]["season"];
            let winner = array1[index]["winner"];
            
            if( team.hasOwnProperty(season)){
                if ( team[season].hasOwnProperty(winner)){
                    team[season][winner] += 1;
                }
                else{
                    team[season][winner] = 1;
                }
            }
            else{
                team[season] = {};
                team[season][winner] = 1;
            }
            index++
    }
    return  team;
}
    FileSystem.writeFileSync("matches.json",JSON.stringify(json),"utf-8",(err)=>{
        if(err) console.log(err)
})

    console.log("question no 2:",players_won())
})



